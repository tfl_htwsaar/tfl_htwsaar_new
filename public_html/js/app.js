'use strict';

// application main module, which depends on appControllers and ngRoutes
angular.module('TflHtwSaarApp', [
    'ngRoute'

]).config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'pages/home.html'
        }).otherwise({
            redirectTo: '/home'
        });
    }]);