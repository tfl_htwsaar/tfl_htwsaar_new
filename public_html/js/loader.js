
function startMapLoader() {
    document.getElementById("loader").style.display = "block";
    document.getElementById("map").style.opacity = "0.2";
}

function stopMapLoader() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("map").style.opacity = "1.0";
}

function startLoader(id) {
    document.getElementById(id).style.display = "block";
}

function stopLoader(id) {
    document.getElementById(id).style.display = "none";
}