var appControllers = angular.module('TflHtwSaarApp');

/* This service provides an interface to the stopFunctions */

appControllers.service('stopService', function ($http, $q, mapService) {
    // Gets the lines in a given radius
    this.getLinesInRadius = function(lat, lon, radius) {
        // returns a promise so we can check if it was successful or failed
        return $q(function (resolve, reject) {
            var myArray = [];
            $http.get("https://api.tfl.gov.uk/StopPoint?lat=" + lat + "&lon=" + lon + "&stopTypes=NaptanPublicBusCoachTram&radius=" + radius)
                    .then(function (response) {
                        var myData = response.data.stopPoints; // get the stoppoints where we get the lines form

                        // running through the stoppoints
                        angular.forEach(myData, function (stops, key) {
                            // for each line
                            angular.forEach(stops.lines, function (line, key) {
                                // check if the line is not in the list yet
                                if (!myArray.includes(line.id)) {
                                    myArray.push(line.id); // add line to the list
                                }
                            });
                        });
                        myArray.sort(); // sort the array with the lines
                        resolve(myArray); // add the lines to the scope	
                    });
        }, 2000);
    }


    // direction: {inbound, outbound} ; dayTime: {Regular, Night}
    this.getLineStops = function (lineNumber, direction, dayTime) {
        // returns a promise so we can check if it was successful or failed
        return $q(function (resolve, reject) {
            setTimeout(function () {
                var stopPoints = [];
                // Get lines in the given order
                $http.get("https://api.tfl.gov.uk/Line/" + lineNumber + "/Route/Sequence/" + direction + "?serviceTypes=" + dayTime)
                        .then(function (response) {
                            var stopPointsJson = response.data.stopPointSequences[0].stopPoint;

                            // run through the stops and prepare them for the return value
                            angular.forEach(stopPointsJson, function (stop) {
                                var newStop = {id: stop.id, name: stop.name, stopLetter: stop.stopLetter, lon: stop.lon, lat: stop.lat};
                                stopPoints.push(newStop);
                            });
                            resolve(stopPoints); // return stops
                        });
            }, 2000);
        });
    }


    // Gets all stops for a given line in both directions
    this.getStops = function(lineNumber) {
        // get bus stops asynchronicly
        startMapLoader(); // start the loader
        var promise = $q.defer();
        var promise2 = $q.defer();

        promise = this.getLineStops(lineNumber, "inbound", "Regular");
        promise.then(function (result) { // success
            angular.forEach(result, function (stop) {
                // create the markers and add them to the group on the layer
                var marker = L.marker([stop.lat, stop.lon], {icon: mapService.greenIcon}); // add the marker to the map with the lon and lat of the station
                marker.options.stop = stop.id;
                marker.options.name = stop.name;
                marker.bindPopup('<div id="popupLoader"></div>', {maxHeight: 300}).openPopup();
                marker.closePopup(); // closes the popups
                mapService.currentStopList.addLayer(marker);
            });
        });
        promise2 = this.getLineStops(lineNumber, "outbound", "Regular");
        promise2.then(function (result) { // success
            angular.forEach(result, function (stop) {
                // create the markers and add them to the group on the layer
                var marker = L.marker([stop.lat, stop.lon], {icon: mapService.redIcon}); // add the marker to the map with the lon and lat of the station
                marker.options.stop = stop.id;
                marker.options.name = stop.name;
                marker.bindPopup('<div id="popupLoader"></div>', {maxHeight: 300}).openPopup();    // make a popup with the name of the station
                marker.closePopup(); // closes the popups
                mapService.currentStopList.addLayer(marker);
            });
        });

        // when the two promises are finished then stop the loader and add the markers to the map
        $q.all([promise, promise2]).then(function() {
            stopMapLoader();
            mapService.currentStopList.addTo(mapService.map);
        });
    }





});