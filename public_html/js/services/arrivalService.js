var appControllers = angular.module('TflHtwSaarApp');

/* This service provides an interface to the arrival functions */

appControllers.service('arrivalService', function ($http, $q) {

    // this function returns a promise with the arrival times in form of an array from the stop with the id given as parameter
    this.getArrivalTimes = function (stopId) {
        return $q(function (resolve, reject) {
            var list = [];
            $http.get("https://api.tfl.gov.uk/StopPoint/" + stopId) // get all lines which cross this stop
                .then(function (response) {
                    var promises = []; // list of promises
                    $selectedData = []; // list of times
                    angular.forEach(response.data.lines, function (line) { // foreach line
                        var promise = $http.get("https://api.tfl.gov.uk/Line/" + line.id + "/Arrivals?stopPointId=" + stopId)
                            .then(function (response) {
                                angular.forEach(response.data, function (value) { // foreach arrival time
                                    $time = value.expectedArrival.substring(11, 19);

                                    var today = new Date();
                                    var arrivalTime = new Date(value.expectedArrival);
                                    var diffMs = (arrivalTime - today);
                                    var diffDays = Math.round(diffMs / 86400000); // days
                                    var diffHrs = Math.round((diffMs % 86400000) / 3600000); // hours
                                    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

                                    if (diffHrs < 10) { diffHrs = "0" + diffHrs }
                                    if (diffMins < 10) { diffMins = "0" + diffMins }
                                    
                                    // create a custom item object for the time
                                    $item = { line: line.id, diffHrs: diffHrs, diffMins: diffMins, time: diffHrs + " hours, " + diffMins + " minutes", towards: value.towards };

                                    $selectedData.push($item);
                                });
                            });
                        promises.push(promise); // add the http request promise to the list of promises
                    });

                    // if all http requests are finished 
                    $q.all(promises)
                        .then(function (result) {
                            // the list will be sorted from the most recent bus to the farest bus
                            $selectedData.sort(function(a,b) {
                                if (a.diffHrs > b.diffHrs) {
                                    return 1;
                                } else if (a.diffHrs < b.diffHrs) {
                                    return -1;
                                } else {
                                    if (a.diffMins > b.diffMins) {
                                        return 1;
                                    } else {
                                        return -1;
                                    }
                                }
                            });

                            resolve($selectedData); // return times as a promise
                        });
                });
        });
    }
});