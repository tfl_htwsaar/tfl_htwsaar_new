var appControllers = angular.module('TflHtwSaarApp');

/* This service provides an interface to the map */

appControllers.service('mapService', function () {
    // This sets the map at the correct location with the correct zoom level
    this.map = L.map('map', {zoomControl: false}).setView([LAT, LON], 12);
    L.control.zoom({position: 'topright'}).addTo(this.map);
    // This is a new layer for the markers. It groups them together and it is easier to remove them later.
    this.currentStopList = L.layerGroup();

    // Leaflet layer :  shows map of london , it's routes and streets.
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'marckirsch.038jllml',
        accessToken: 'pk.eyJ1IjoibWFyY2tpcnNjaCIsImEiOiJjaW8wZDJhbmUwMHY5dmRseWtmMnFkMmQxIn0.EmvVvvi7LyHmM1YuuVgBqA'
    }).addTo(this.map);

    this.greenIcon = L.icon({
        iconUrl: 'img/leaf-green.png',
        shadowUrl: 'img/leaf-shadow.png',
        iconSize: [38, 95], // size of the icon
        shadowSize: [50, 64], // size of the shadow
        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
        shadowAnchor: [4, 62], // the same for the shadow
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    this.redIcon = L.icon({
        iconUrl: 'img/leaf-red.png',
        shadowUrl: 'img/leaf-shadow.png',
        iconSize: [38, 95], // size of the icon
        shadowSize: [50, 64], // size of the shadow
        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
        shadowAnchor: [4, 62], // the same for the shadow
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
});