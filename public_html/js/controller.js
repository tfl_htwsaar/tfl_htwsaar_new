// Controller Module : loads all application controllers
var appControllers = angular.module('TflHtwSaarApp');
var LON = -0.09;
var LAT = 51.505;
/* global google */

// custom services
appControllers.controller('mainController', function ($http, $q, arrivalService, stopService, mapService) {

    // This function will be called by the buttons to show the markers of the stops for that line
    this.showLine = function ($event) {
        var me = $event.currentTarget; // get the clicked button
        mapService.currentStopList.clearLayers(); // removes all the old markers
        stopService.getStops(me.innerHTML); // gets the stops and add them to the layer/map
    };

    // does it after finish loadeing of the page
    angular.element(document).ready(function () {
        // add click events for the menu items, so the sliders will show or hide correctly

        document.getElementById('Location').onclick = function () {
            if (document.getElementById('show_Location').style.display == 'block') {
                document.getElementById('show_Location').style.display = 'none';
            } else {
                document.getElementById('show_Location').style.display = 'block';
            }
            document.getElementById('show_BusLine').style.display = 'none';
            document.getElementById('show_BusStop').style.display = 'none';
            document.getElementById('show_BusLoc').style.display = 'none';
            document.getElementById('show_Journey').style.display = 'none';
        };
        
           document.getElementById('closeButton0').onclick = function () {
            if (document.getElementById('show_Location').style.display == 'block') {
                document.getElementById('show_Location').style.display = 'none';
            } else {
                document.getElementById('show_Location').style.display = 'block';
            }
        };


        document.getElementById('BusLine').onclick = function () {
            if (document.getElementById('show_BusLine').style.display == 'block') {
                document.getElementById('show_BusLine').style.display = 'none';
            } else {
                document.getElementById('show_BusLine').style.display = 'block';
            }
            document.getElementById('show_Location').style.display = 'none';
            document.getElementById('show_BusStop').style.display = 'none';
            document.getElementById('show_BusLoc').style.display = 'none';
            document.getElementById('show_Journey').style.display = 'none';
        };
        
          document.getElementById('closeButton1').onclick = function () {
            if (document.getElementById('show_BusLine').style.display == 'block') {
                document.getElementById('show_BusLine').style.display = 'none';
            } else {
                document.getElementById('show_BusLine').style.display = 'block';
            }
        };

        document.getElementById('BusStop').onclick = function () {
            if (document.getElementById('show_BusStop').style.display == 'block') {
                document.getElementById('show_BusStop').style.display = 'none';
            } else {
                document.getElementById('show_BusStop').style.display = 'block';
            }
            document.getElementById('show_Location').style.display = 'none';
            document.getElementById('show_BusLine').style.display = 'none';
            document.getElementById('show_BusLoc').style.display = 'none';
            document.getElementById('show_Journey').style.display = 'none';
        };
        
          document.getElementById('closeButton2').onclick = function () {
            if (document.getElementById('show_BusStop').style.display == 'block') {
                document.getElementById('show_BusStop').style.display = 'none';
            } else {
                document.getElementById('show_BusStop').style.display = 'block';
            }
        };

        document.getElementById('BusLoc').onclick = function () {
            if (document.getElementById('show_BusLoc').style.display == 'block') {
                document.getElementById('show_BusLoc').style.display = 'none';
            } else {
                document.getElementById('show_BusLoc').style.display = 'block';
            }
            document.getElementById('show_Location').style.display = 'none';
            document.getElementById('show_BusLine').style.display = 'none';
            document.getElementById('show_BusStop').style.display = 'none';
            document.getElementById('show_Journey').style.display = 'none';
        };


              document.getElementById('closeButton3').onclick = function () {
            if (document.getElementById('show_Journey').style.display == 'block') {
                document.getElementById('show_Journey').style.display = 'none';
            } else {
                document.getElementById('show_Journey').style.display = 'block';
            }
        };
        
        document.getElementById('Journey').onclick = function () {
            if (document.getElementById('show_Journey').style.display == 'block') {
                document.getElementById('show_Journey').style.display = 'none';
            } else {
                document.getElementById('show_Journey').style.display = 'block';
            }
            document.getElementById('show_Location').style.display = 'none';
            document.getElementById('show_BusLine').style.display = 'none';
            document.getElementById('show_BusStop').style.display = 'none';
            document.getElementById('show_BusLoc').style.display = 'none';
        };

         document.getElementById('closeButton4').onclick = function () {
            if (document.getElementById('show_BusLoc').style.display == 'block') {
                document.getElementById('show_BusLoc').style.display = 'none';
            } else {
                document.getElementById('show_BusLoc').style.display = 'block';
            }
        };
        // shows the correct elements for the different radio items
        $('input[type="radio"]').click(function () {
            if ($(this).attr("value") === "Line") {
                $(".selectRadius").hide();
                $(".selectLine").show();
            }
            if ($(this).attr("value") === "Radius") {
                $(".selectLine").hide();
                $(".selectRadius").show();
            }
        });

        // load the lines for the select tags
        var lines;
        var promise = stopService.getLinesInRadius(LAT, LON, 1000); // get lines within 1000m radius
        promise.then(function (result) { // success
            lines = result;
            var select = document.getElementsByName("cbBusLine"); // get select tags
            select.forEach(function (selectElement) { // for all select tag
                lines.forEach(function (element) { // for all lines
                    var option = document.createElement("option");
                    option.text = element;
                    option.value = element;
                    selectElement.add(option); // add option to the select
                }, this);
            }, this);
        }, function () { // failed
            console.log("Lines couldn't be loaded!");
        });

        // show the correct bus line on the map
        document.getElementById("btnBusStopSearch").onclick = function () {
            // get bus stops asynchronicly 
            mapService.currentStopList.clearLayers(); // clear the map
            stopService.getStops(document.getElementById("busStopSelect").value); // get the buses and set them on the map
            document.getElementById("show_BusStop").style.display = "none"; // hide the busStop panel
        }

        // show the busline 
        document.getElementById("btnBusLineSearch").onclick = function () {
            mapService.currentStopList.clearLayers(); // clear the map
            // draw the routes in both directions
            drawRoute(document.getElementById("busLineSelect").value, "inbound"); 
            drawRoute(document.getElementById("busLineSelect").value, "outbound");
            document.getElementById("show_BusLine").style.display = "none"; // hide the panel
        }
    });

    // when popup opens load arrival times
    mapService.map.on('popupopen', function(e) {
        startLoader("popupLoader"); // start loader
        var arrivals = arrivalService.getArrivalTimes(e.popup._source.options.stop); // get arrival times from service
        var content = "";
        
        // when finished loading
        arrivals.then(function(result) {
            // create display table
            content = '<h4>'+ e.popup._source.options.name +'</h4><table id="arrivalTable" class="table table-striped" border="1"><thead><th>expected arrival</th><th>towards</th></thead><tbody>';
            angular.forEach(result, function(value) { 
                content = content + "<tr><td>"+ value.time +"</td><td>"+ value.towards +"</td></tr>";
            });
            content = content + '</tbody></table>';
            e.popup.setContent(content); // add table to the popup
        });
    });


    // lineNumber = number of the bus line ; direction = inbound or outbound
    drawRoute = function (lineNumber, direction) {
        var latlngArray = []; // array of the final coordinates
        $http.get("https://api.tfl.gov.uk/Line/" + lineNumber + "/Route/Sequence/" + direction) // get route coordinates
                .then(function (response) {
                    var trackArray = JSON.parse(response.data.lineStrings); // json string to array of the  coordinates
                    var log = [];
                    angular.forEach(trackArray[0], function (value, key) { // for each coordinate
                        var latlng = L.latLng(value); // make the coordinate in the correct form
                        var truelatlng = [latlng.lng, latlng.lat];
                        latlngArray.push(truelatlng); // add to the final array
                    }, log);

                    // inbound = green ; outbound = red
                    if (direction == "inbound") {
                        var polyline = L.polyline(latlngArray, {color: 'green'}); // "draw" the polyline between the given latlng-points
                    } else {
                        var polyline = L.polyline(latlngArray, {color: 'red'}); // "draw" the polyline between the given latlng-points
                    }
                    mapService.currentStopList.addLayer(polyline); // add line to the layergroup
                    mapService.currentStopList.addTo(mapService.map); // add layergroup to map
                });
    };
    
    function planJourneyStart(startStation, destinationStation) {
        return $q(function (resolve, reject) {
            myArray = [];   
            $http.get("https://api.tfl.gov.uk/journey/journeyresults/" + startStation + "/to/" + destinationStation + "?journeyPreference=LeastWalking")//get journey
                .then(function (response) {
                    
                    var log = [];
                    angular.forEach(response.data.journeys, function (value, key) {
                        myArray.push(value.startDateTime);  //get start times in array      
                    },log);
                    
                   resolve(myArray);
                });
                
            }, 2000);
    };
    
    function planJourneyEnd(startStation, destinationStation) {
        return $q(function (resolve, reject) {
            myArray2 = [];
            $http.get("https://api.tfl.gov.uk/journey/journeyresults/" + startStation + "/to/" + destinationStation + "?journeyPreference=LeastWalking")//get journey
                .then(function (response) {
                    
                    var log = [];
                    angular.forEach(response.data.journeys, function (value, key) {
                        myArray2.push(value.arrivalDateTime);   //get arrival times in array     
                    },log);
                    
                   resolve(myArray2);
                });
            }, 2000);    
        
    };
    
    // load the lines for the select tags
    
    function fillTable(start, end) {
        var first = start;
        var second = end;
        var start_coord;
        var end_coord;
        var times;
        cellArray = [];
        
        //change given stop-names to needed coordinates(lat,lon) (start) 
        var promise = getCoordinates(first);
        promise.then(function (result) {
            times = result;
            
                times.forEach(function (element) {
                    start_coord = element;
                },this);
       
       //change given stop-names to needed coordinates(lat,lon) (destination)
        var promise2 = getCoordinates(second);
        promise2.then(function (result) {
            times = result;
            
                times.forEach(function (element) {
                    end_coord = element;
                },this);                
        
        //fill the table with the start-times    
        var promise3 = planJourneyStart(start_coord,end_coord); // get start times
        promise3.then(function (result) { // success
            times = result;
            var tableRef = document.getElementById('testName').getElementsByTagName('tbody')[0]; // select table
            
                times.forEach(function (element) { 
                    
                    // Insert a row in the table at row index 0
                    var newRow   = tableRef.insertRow(tableRef.rows.length);

                    // Insert a cell in the row at index 0+1 and push the 2nd to an array for later use
                    var newCell  = newRow.insertCell(0);
                    newCell2 = newRow.insertCell(1);
                    cellArray.push(newCell2);
                    
                    // Append a text node to the cell
                    var newText  = document.createTextNode(element);
                    newCell.appendChild(newText);

            }, this);
        
        //fill the table with the arrival-times 
        var i = 0;
        var promise4 = planJourneyEnd(start_coord, end_coord); // get arrvival times
        promise4.then(function (result) { // success
            times = result;
            
                times.forEach(function (element) {

                    // Get the cell
                    var cell = cellArray[i++];
                    
                    // Append a text node to the cell
                    var newText  = document.createTextNode(element);
                    cell.appendChild(newText);

            }, this);
        }, function () { // failed
            console.log("Arrivals couldn't be loaded!");
        });    
            
        });
        });
        });
    }
    
    //function to change the stop-names to coordinates
    function getCoordinates(station) {
       return $q(function (resolve, reject) { 
           $http.get("https://api.tfl.gov.uk/StopPoint/Search/" + station) //information about the given stop (incl. lat+lon)
                .then (function(response){
                                        
                    var myArray = response.data.matches;
                    myArray2 = [];
                    
                    var log = [];
                    angular.forEach(myArray, function (value, key) {
                        
                        //get lat+lon
                        var lat = value.lat;
                        var lon = value.lon;
                        
                        //convert to string
                        var lat_str = lat.toString();
                        var lon_str = lon.toString();
                        var coordinate = lat_str + "," + lon_str;
                        myArray2.push(coordinate);
                   },log);
                   
                resolve(myArray2);
           });   
       }, 2000);
    };
    
    //clearing the table for journey planner
    function clearTable() {
        var x = document.getElementById('testName').getElementsByTagName('tbody')[0];
        
        while(x.rows.length > 0) {
            x.deleteRow(0);
        }
    }
    
    document.getElementById("btnJourneyPlanner").onclick = function () {
            clearTable();
            //get the start + destination stops
            var x = document.getElementById("start").value;
            var y = document.getElementById("destination").value;
            fillTable(x, y);//"51.460312,-0.011235" "51.549715, -0.084649"         
        };     
    
    $("#btnSearchLocation").click(function(){
        var geocoder = new google.maps.Geocoder();
        // store the given addres of the user
        var getAddress = $("#inputLocation").val();
        
        // This is making the Geocode request
        geocoder.geocode({'address':getAddress}, function(results,status){
            if (status === google.maps.GeocoderStatus.OK){
                var marker = new google.maps.Marker({
                    position: results[0].geometry.location
                });
                
                // store the lat and lng coordinates of the address in hidden DIV's
                $("#lat").text(marker.getPosition().lat());
                $("#long").text(marker.getPosition().lng());
                
                var lat = document.getElementById("lat").innerHTML;
                var long = document.getElementById("long").innerHTML;
                
                setMarker(lat,long);
            }else{
                $(".inputLocation").val("Please enter a correct location!");
            }
        });
    });
    
    function setMarker(lat,long){
        /* take converted coordinates from the DIV to use them to point a marker */
        mapService.currentStopList.clearLayers();
        var marker = L.marker([lat,long]);
        mapService.currentStopList.addLayer(marker);
        mapService.currentStopList.addTo(mapService.map);
        /*var marker = L.marker([lat,long]);
        marker.addTo(mapService.map);*/
    }
});

