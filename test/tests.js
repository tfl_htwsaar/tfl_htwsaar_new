/* TEST FOR APP.JS */

'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('TflHtwSaarApp', function() {
    it('should automatically redirect to index.html', function() {
        browser.get('index.html');
        expect(browser.getLocationAbsUrl()).toMatch("index.html");
    });

    describe('View: /home',function(){
        beforeEach(function() {
            browser.get('pages/home.html');
        });
    });
});

/* TEST FOR CONTROLLER.JS */

describe('Controller.js',function(){
	beforeEach(module('TflHtwSaarApp'));

	var $controller;

	beforeEach(inject(function(_$controller_){
		// The injector unwraps the underscores (_) from around the parameter names when matching
		$controller = _$controller_;
	}));

	describe('this.showLine($event)',function(){

	});

	describe('angular.element(document).ready(function ()',function(){
		describe('document.getElementById('Location').onclick = function ()',function(){
		});

		describe('document.getElementById('Location').onclick = function ()',function(){
		});

		describe('document.getElementById('BusLine').onclick = function ()',function(){
		});

		describe('document.getElementById('BusStop').onclick = function ()',function(){
		});

		describe('document.getElementById('BusLoc').onclick = function ()',function(){
		});

		describe('document.getElementById('Journey').onclick = function ()',function(){
		});

		describe('radio button',function(){
		});

		describe('loading the lines',function(){
		});

		describe('document.getElementById("btnBusStopSearch").onclick = function ()',function(){

    });

		describe('document.getElementById("btnBusLineSearch").onclick = function ()',function(){
      
    });

	});

	describe('mapService.map.on()',function(){
	});

	describe('drawRoute',function(){
	});

	describe('planJourneyStart',function(){
	});

	describe('planJourneyEnd',function(){
	});

	describe('fillTable',function(){
	});

	describe('getCoordinates',function(){
	});

	describe('clearTable',function(){
	});

	describe('document.getElementById("btnJourneyPlanner").onclick = function ()',function(){
	});

	// Test für das Platzieren von einem Marker auf eine angegebene Location
	describe('$("#btnSearchLocation").click(function()',function(){
		var clickedButton = element(byid('btnSearchLocation'));
		clickedButton.click();

        var GivenLocation = element(byid('inputLocation'));
		GivenLocation.setText('London');
		var LAT,LON;

		it ('should be enterred London in the input field',function(){
			expect(GivenLocation.getText()).equalTo('London');

			expect(LAT).equalTo(51.500083);
			expect(LON).equalTo(-0.126182);
		});
	});

	describe('setMarker',function(){
	});
});

/* TEST FOR LOADER.JS */
describe('loader.js',function(){
	describe('startMapLoader',function(){
	});
	describe('stopMapLoader',function(){
	});
	describe('startLoader',function(){
	});
	describe('stopLoader',function(){
	});
});


/* arrivalService.js */
describe('arrivalService.js',function(){
	beforeEach(module('arrivalService'));

	describe('getArrivalTimes',function(){
	});
});

/* mapService.js */
describe('mapService.js',function(){
	beforeEach(module('mapService'));

});

/* stopService.js */
describe('stopService.js',function(){
	beforeEach(module('stopService'));

	describe('getLinesInRadius',function(){
	});

	describe('getLinesStops',function(){
	});

	describe('getStops',function(){
	});

});
